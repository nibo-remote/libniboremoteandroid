/*
 * Copyright (C) 2013-2014 Tobias Ilte
 *
 * This file is part of libNiboRemote.
 *
 * libNiboRemote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libNiboRemote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libNiboRemote. If not, see <http://www.gnu.org/licenses/>.
 */

package de.thwildau.libniboremoteandroid;

import java.lang.Math;

/**
 * Defines macros, which let you easily execute commands known from nibolib.
 * 
 * @author Tobias Ilte
 */
public class NiboProtocolHelper {
	private NiboProtocolParser mNiboProtocolParser;

	public static final byte LEDS_OFF = 0;
	public static final byte LEDS_GREEN = 1;
	public static final byte LEDS_RED = 2;
	public static final byte LEDS_ORANGE = 3;

	public enum registerNames{
		/* System */
		NSPREG_BOTID,
		NSPREG_VERSION,
		NSPREG_VSUPPLY,
		/* LEDs */
		NSPREG_LEDSR,
		NSPREG_LEDSG,
		NSPREG_LEDSWPWM,
		/* Motor */
		NSPREG_MOTMODE,
		NSPREG_MOTPWML,
		NSPREG_MOTPWMR,
		NSPREG_MOTPIDL,
		NSPREG_MOTPIDR,
		NSPREG_MOTCURL,
		NSPREG_MOTCURR,
		/* Odometry */
		NSPREG_ODOL,
		NSPREG_ODOR,
		/* Floor/Line */
		NSPREG_FLOORL,
		NSPREG_FLOORR,
		NSPREG_LINEL,
		NSPREG_LINER,
		/* Distance */
		NSPREG_DIST_L,
		NSPREG_DIST_FL,
		NSPREG_DIST_F,
		NSPREG_DIST_FR,
		NSPREG_DIST_R,
		NSPREG_DIST_NDS3
	};
	
	
	public NiboProtocolHelper(NiboProtocolParser niboProtocolParser) {
		mNiboProtocolParser = niboProtocolParser;
		;
	}

	public void leds_set_status(byte color, byte led) {
		byte ledBinary = (byte) Math.pow(2, led);
		switch (color) {
			case LEDS_OFF:
				mNiboProtocolParser.registerFlags[registerNames.NSPREG_LEDSR.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
				mNiboProtocolParser.registerFlags[registerNames.NSPREG_LEDSG.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
				mNiboProtocolParser.registerValues[registerNames.NSPREG_LEDSR.ordinal()] &= ~ledBinary;
				mNiboProtocolParser.registerValues[registerNames.NSPREG_LEDSG.ordinal()] &= ~ledBinary;
				break;
			case LEDS_GREEN:
				mNiboProtocolParser.registerFlags[registerNames.NSPREG_LEDSG.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
				mNiboProtocolParser.registerValues[registerNames.NSPREG_LEDSG.ordinal()] |= ledBinary;
				break;
			case LEDS_RED:
				mNiboProtocolParser.registerFlags[registerNames.NSPREG_LEDSR.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
				mNiboProtocolParser.registerValues[registerNames.NSPREG_LEDSR.ordinal()] |= ledBinary;
				break;
			case LEDS_ORANGE:
				mNiboProtocolParser.registerFlags[registerNames.NSPREG_LEDSR.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
				mNiboProtocolParser.registerFlags[registerNames.NSPREG_LEDSG.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
				mNiboProtocolParser.registerValues[registerNames.NSPREG_LEDSG.ordinal()] |= ledBinary;
				mNiboProtocolParser.registerValues[registerNames.NSPREG_LEDSR.ordinal()] |= ledBinary;
				break;
			default:
				break;
		}
	}

	public void leds_set_headlights(int light) {
		mNiboProtocolParser.registerFlags[registerNames.NSPREG_LEDSWPWM.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
		mNiboProtocolParser.registerValues[registerNames.NSPREG_LEDSWPWM.ordinal()] = light;
	}

	public void copro_stopImmediate() {
		mNiboProtocolParser.registerFlags[registerNames.NSPREG_MOTMODE.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
		mNiboProtocolParser.registerValues[registerNames.NSPREG_MOTMODE.ordinal()] = 0;
	}

	public void copro_stop() {
		mNiboProtocolParser.registerFlags[registerNames.NSPREG_MOTMODE.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
		mNiboProtocolParser.registerValues[registerNames.NSPREG_MOTMODE.ordinal()] = 1;
	}

	public void copro_setPWM(int left, int right) {
		mNiboProtocolParser.registerFlags[registerNames.NSPREG_MOTMODE.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
		mNiboProtocolParser.registerFlags[registerNames.NSPREG_MOTPWML.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
		mNiboProtocolParser.registerFlags[registerNames.NSPREG_MOTPWMR.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
		mNiboProtocolParser.registerValues[registerNames.NSPREG_MOTMODE.ordinal()] = 2;
		mNiboProtocolParser.registerValues[registerNames.NSPREG_MOTPWML.ordinal()] = left;
		mNiboProtocolParser.registerValues[registerNames.NSPREG_MOTPWMR.ordinal()] = right;
	}

	public void copro_setSpeed(int left, int right) {
		mNiboProtocolParser.registerFlags[registerNames.NSPREG_MOTMODE.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
		mNiboProtocolParser.registerFlags[registerNames.NSPREG_MOTPIDL.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
		mNiboProtocolParser.registerFlags[registerNames.NSPREG_MOTPIDR.ordinal()] |= NiboProtocolParser.NSP_FLAG_SET;
		mNiboProtocolParser.registerValues[registerNames.NSPREG_MOTMODE.ordinal()] = 3;
		mNiboProtocolParser.registerValues[registerNames.NSPREG_MOTPIDL.ordinal()] = left;
		mNiboProtocolParser.registerValues[registerNames.NSPREG_MOTPIDR.ordinal()] = right;
	}
}
