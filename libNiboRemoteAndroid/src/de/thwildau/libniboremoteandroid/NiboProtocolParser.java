/*
 * Copyright (C) 2013-2014 Tobias Ilte
 *
 * This file is part of libNiboRemote.
 *
 * libNiboRemote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libNiboRemote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libNiboRemote. If not, see <http://www.gnu.org/licenses/>.
 */

package de.thwildau.libniboremoteandroid;

import java.io.IOException;
import java.io.StringReader;

import android.util.Log;

/**
 * This class parses all requests and replies and saves the data into an array.
 * 
 * @author Tobias Ilte
 */
public class NiboProtocolParser {
	// Used for Debugging
	private static final String TAG = "Nibo Remote(NiboProtocolParser.java)";
	private static final boolean DEBUG = true;

	// Definition of flags for the flag-register
	public static final byte NSP_FLAG_SET = 1;
	public static final byte NSP_FLAG_GET = 2;
	public static final byte NSP_FLAG_REPORT = 4; // automatically get data at every request.
	public static final byte NSP_FLAG_UNREPORT = 8;

	/**
	 * The array "registerValues" contains the sensor data of the robot. Every element of the array represents one
	 * sensor/component of the robot. The numbering is compatible with the Nibo Serial Protocol. E.g.:
	 * registerValues[13] represents the ticks/s of the left motor.
	 */
	public int[] registerValues;

	/**
	 * The array "registerFlags" contains flags, which indicate how data is requested. It has the same numbering as
	 * registerValues. So f.ex. when you want to set the speed of the left motor to 10 ticks/s, you simply call: <br>
	 * {@code registerFlags[13] |= NSP_FLAG_SET;}<br>
	 * {@code registerValues[13] = 10;}<br>
	 * Finally submit the request: {@code submitData();}
	 */
	public byte[] registerFlags;

	public NiboProtocolParser(int registerSize) {
		this.registerValues = new int[registerSize];
		this.registerFlags = new byte[registerSize];
	}

	/** Parses the complete Answer-String. */
	public void parseAnswer(String answerString) {
		StringReader mStringReader = new StringReader(answerString);
		int i = 0;
		while (i != -1) {
			try {
				i = mStringReader.read();
				if (DEBUG) Log.d(TAG, "parsing(parseAnswer: " + Integer.toString(i));
				switch (i) {
					case 42: // == *
						continue;
					case 33: // == !
						parseSet(mStringReader);
						break;
					default:
						break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/** Parses the set-reply from the robot. */
	private void parseSet(StringReader mStringReader) throws IOException {
		int asciiValue = 0;
		int register = 0;
		int registerValue = 0;
		register = parseHex(mStringReader);
		mStringReader.mark(0);
		asciiValue = mStringReader.read();
		if (asciiValue == 44) { // ascii-value for comma (",")
			registerValue = parseHex(mStringReader);
		}
		registerValues[register] = registerValue;
	}

	/** Parses a number as String into a hexadecimal-number */
	private int parseHex(StringReader mStringReader) throws IOException {
		int i = 0;
		String str = "";
		while (true) {
			mStringReader.mark(0);
			i = mStringReader.read();
			if (DEBUG) Log.d(TAG, "parsing(parseHex: " + Integer.toString(i));
			// ASCII-Char has to be Hexadecimal (0-9, a-f)
			if (((i > 47) && (i < 58)) || ((i > 96) && (i < 103))) {
				str += (char) i;
			} else {
				mStringReader.reset();
				break;
			}
		}
		if (str.length() > 0) {
			return Integer.parseInt(str, 16);
		} else {
			return 0;
		}
	}

	/** Builds String from flags in the registerFlags-array. */
	public String buildRequest() {
		String s = "$";
		for (int i = 0; i < registerFlags.length; i++) {
			switch (registerFlags[i]) {
				case NSP_FLAG_SET:
					s += "!" + Integer.toHexString(i) + "," + Integer.toHexString(registerValues[i]);
					registerFlags[i] &= ~NSP_FLAG_SET;
					break;
				case NSP_FLAG_GET:
					s += "?" + Integer.toHexString(i);
					registerFlags[i] &= ~NSP_FLAG_GET;
					break;
				case NSP_FLAG_REPORT:
					s += "#" + Integer.toHexString(i);
					registerFlags[i] &= ~NSP_FLAG_REPORT;
					break;
				case NSP_FLAG_UNREPORT:
					s += "~" + Integer.toHexString(i);
					registerFlags[i] &= ~NSP_FLAG_UNREPORT;
					break;
				default:
					break;
			}
		}
		if (DEBUG) Log.d(TAG, "Request: " + s);
		return (s + "\n");
	}
}